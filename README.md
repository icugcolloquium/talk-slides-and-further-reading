# Slides for talks 

This repository has all slides for talks, either in the form of files donated by the respective presenters, or as links to where you can find their slides. The repository license allows for distribution with attribution but no modification, commericialistion, etc. For more details, read the license.

# Further reading

In addition, we include further reading resources. This is in the form of lists of resources provided by presenters. They will become available here before the talks, and on [our website](https://icugcolloquium.codeberg.page/) before the talk, in case you desire to prepare for the subject ahead.